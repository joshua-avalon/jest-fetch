# Jest Fetch

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm]

jest-fetch allows you to mock fetch response in jest.

## Installation

```
npm i jest-fetch
```

## Usage

> The following examples are based on TypeScript but JavaScript will work fine.

First, you to add setup script. In `jest.config.js`, add this following configuration.

```javascript
setupFiles: ["./test/setup.ts"];
```

**setup.ts**

```typescript
import { fetch } from "jest-fetch";

global.fetch = fetch;
```

You replace the global fetch with jest-fetch. Note that you have to used global fetch in the first place for it to work (e.g. `isomorphic-fetch`).

**example.test.ts**

```typescript
import { mockResponse, resetMocks } from "jest-fetch";

beforeEach(() => {
  resetMocks();
});

test("Test mock string response", async () => {
  const expectResponse = "!!!";
  mockResponse(expectResponse);

  const response = await fetch("https://example.com").then(res => res.text());

  expect(response).toBe(expectResponse);
});
```

You need to call `mockImplementationOnce` or `mockResponse` before your fetch requests.
You can pass the response directly or a function.

```typescript
mockResponse(async (url, init) => {
  return "content";
});
```

`url` and `init` are the variables that you passed to fetch.

## Differences from Jest Fetch Mock

[jest-fetch-mock](https://www.npmjs.com/package/jest-fetch-mock) [does not support you to mock response based URL.](https://github.com/jefflau/jest-fetch-mock/issues/52) This is the reason of this package is created.

[license]: https://gitlab.com/joshua-avalon/jest-fetch/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/joshua-avalon/jest-fetch/pipelines
[pipelines_badge]: https://gitlab.com/joshua-avalon/jest-fetch/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/joshua-avalon/jest-fetch/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/jest-fetch
[npm_badge]: https://img.shields.io/npm/v/jest-fetch/latest.svg
