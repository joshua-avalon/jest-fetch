import { mockResponse, mockResponseOnce, resetMocks } from "../src";

beforeEach(() => {
  resetMocks();
});

test("Test mock string response", async () => {
  const expectResponse = "!!!";
  mockResponse(expectResponse);

  const response = await fetch("https://example.com").then(res => res.text());

  expect(response).toBe(expectResponse);
});

test("Test mock function response", async () => {
  const url = "https://example.com";
  mockResponse(async url => (typeof url === "string" ? url : ""));

  const response = await fetch(url).then(res => res.text());

  expect(response).toBe(url);
});

test("Test mock string response once", async () => {
  const expectResponse1 = "!!!";
  const expectResponse2 = "???";
  mockResponseOnce(expectResponse1);
  mockResponseOnce(expectResponse2);

  const response1 = await fetch("https://example.com").then(res => res.text());
  expect(response1).toBe(expectResponse1);
  const response2 = await fetch("https://example.com").then(res => res.text());
  expect(response2).toBe(expectResponse2);
});

test("Test mock function response once", async () => {
  const url = "https://example.com";
  const expectResponse = "!!!";
  mockResponseOnce(async url => (typeof url === "string" ? url : ""));
  mockResponseOnce(async () => expectResponse);

  const response1 = await fetch(url).then(res => res.text());
  expect(response1).toBe(url);
  const response2 = await fetch(url).then(res => res.text());
  expect(response2).toBe(expectResponse);
});
