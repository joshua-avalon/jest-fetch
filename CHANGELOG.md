# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.1]

### Added

- Add `@babel/runtime`.

## [1.1.0]

### Added

- Expose jest mock function as `mockFetch`.

### Changed

- Update README.md and description.
- Rename `mockImplementationOnce` to `mockResponseOnce`.

## 1.0.0

Initial Release

[unreleased]: https://gitlab.com/joshua-avalon/jest-fetch/compare/1.1.1...master
[1.1.1]: https://gitlab.com/joshua-avalon/jest-fetch/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.com/joshua-avalon/jest-fetch/compare/1.0.0...1.1.0
