/* eslint-disable @typescript-eslint/no-unused-vars */
import nodeFetch, {
  BodyInit,
  Headers,
  Request,
  RequestInit,
  Response
} from "node-fetch";
/* eslint-enable @typescript-eslint/no-unused-vars */

type fetchFn = typeof nodeFetch;
/* eslint-disable */
declare global {
  namespace NodeJS {
    interface Global {
      fetch: fetchFn;
      Headers: typeof Headers;
      Request: typeof Request;
      Response: typeof Response;
    }
  }
}
/* eslint-enable */

type BodyOrFunction =
  | BodyInit
  | ((url: string | Request, init?: RequestInit) => Promise<BodyInit>);

const fetchMock: jest.Mock<any, any> & {
  isRedirect: (code: number) => boolean;
} = jest.fn() as any;
fetchMock.isRedirect = code => code >= 300 && code < 400;

export const mockFetch: jest.Mock<any, any> = fetchMock;
export const fetch: fetchFn = fetchMock;

async function wrapResponse(
  bodyOrFunction: BodyOrFunction,
  url: string | Request,
  init?: RequestInit
): Promise<Response> {
  if (typeof bodyOrFunction === "function") {
    const result = await bodyOrFunction(url, init);
    return new Response(result);
  }
  return new Response(bodyOrFunction);
}

export function mockResponse(bodyOrFunction: BodyOrFunction): void {
  fetchMock.mockImplementation((url: string | Request, init?: RequestInit) =>
    wrapResponse(bodyOrFunction, url, init)
  );
}

export function mockResponseOnce(bodyOrFunction: BodyOrFunction): void {
  fetchMock.mockImplementationOnce(
    (url: string | Request, init?: RequestInit) =>
      wrapResponse(bodyOrFunction, url, init)
  );
}

export function resetMocks(): void {
  fetchMock.mockReset();
}
